#include "SymbolTable.h"

SymbolTable::SymbolTable() {
	// Symbols
	m_symbols[";"] = TT_SEMICOLON; //Revisar linguagem BNF
	m_symbols["at="] = TT_ASSIGN;
	m_symbols["{"] = TT_START_BLOCK;
	m_symbols["}"] = TT_END_BLOCK;

	// Logic operators
	m_symbols["=="] = TT_EQUAL;
	m_symbols["!="] = TT_NOT_EQUAL;
	m_symbols["<"] = TT_LOWER;
	m_symbols["<="] = TT_LOWER_EQUAL;
	m_symbols[">"] = TT_GREATER;
	m_symbols[">="] = TT_GREATER_EQUAL;

	// Arithmetic operators
	m_symbols["+"] = TT_ADD;
	m_symbols["-"] = TT_SUB;
	m_symbols["*"] = TT_MUL;
	m_symbols["/"] = TT_DIV;
	m_symbols["%"] = TT_MOD;

	// Keywords
	m_symbols["enquanto"] = TT_WHILE;
	m_symbols["se"] = TT_IF;
	m_symbols["escrever"] = TT_OUTPUT;
	m_symbols["verdadeiro"] = TT_TRUE;
	m_symbols["falso"] = TT_FALSE;
	m_symbols["ler"] = TT_READ;
	m_symbols["nao"] = TT_NOT;
	m_symbols["constante"] = TT_CONST;
	m_symbols["inteiro"] = TT_INT;
	m_symbols["flutuante"] = TT_FLOAT;
	m_symbols["caractere"] = TT_CHAR;
	m_symbols["string"] = TT_STR;

}

SymbolTable::~SymbolTable() {
}

bool SymbolTable::contains(const std::string& token) const {
	return m_symbols.find(token) != m_symbols.end();
}

enum TokenType SymbolTable::find(const std::string& token) {
	return this->contains(token) ?
				m_symbols[token] : TT_VAR;
}
